(defproject inspection "0.1.0-SNAPSHOT"
  :description "Requirements management app."
  :url         "https://github.com/Devereux-Henley/inspection"

  ;; Dependencies
  :dependencies [
                 ;; Language dependencies
                 [org.clojure/clojure         "1.9.0-alpha16"]
                 [org.clojure/clojurescript   "1.9.908"]
                 [org.clojure/core.async      "0.3.443"]

                 ;; Build dependencies
                 [binaryage/devtools          "0.9.4"  :scope "test"]
                 [binaryage/dirac             "1.2.15" :scope "test"]
                 [io.rkn/conformity           "0.5.1"  :scope "test"]
                 [lambdaisland/garden-watcher "0.3.2"  :scope "test"]

                 ;; Server Dependencies
                 [aero                        "1.1.2"]
                 [aleph                       "0.4.3"]
                 [bidi                        "2.1.2"]
                 [buddy                       "1.3.0"]
                 [clj-time                    "0.14.0"]
                 [com.datomic/clj-client      "0.8.606"]
                 [com.datomic/datomic-pro     "0.9.5561"
                  :exclusions [com.google.guava/guava]]
                 [hiccup                      "1.0.5"]
                 [manifold                    "0.1.6"]
                 [metosin/ring-swagger        "0.24.1"]
                 [com.stuartsierra/component  "0.3.2"]
                 [org.clojure/tools.namespace "0.3.0-alpha3"]
                 [org.danielsz/system         "0.4.0"]
                 [prismatic/schema            "1.1.6"]
                 [yada                        "1.2.8"
                  :exclusions [aleph commons-fileupload ring-swagger schema core.async]]

                 ;; App Dependencies
                 [com.cognitect/transit-clj "0.8.300"]
                 [compassus                 "1.0.0-alpha3"]
                 [org.clojure/spec.alpha    "0.1.123"]
                 [org.omcljs/om             "1.0.0-beta1"
                  :exclusions [com.fasterxml.jackson.core/jackson-core]]

                 ;; Browser Dependencies
                 [cljs-ajax                 "0.7.0"]
                 [kibu/pushy                "0.3.7"]]

  :repositories {"my.datomic.com" {:url "https://my.datomic.com/repo"
                                   :username :env/DATOMIC_REPO_USERNAME
                                   :password :env/DATOMIC_REPO_PASSWORD}}

  ;; Plugins
  :plugins [[lein-ancient "0.6.10"]
            [lein-cljsbuild "1.1.5"]]

  :min-lein-version "2.7.1"

  :source-paths   ["src"]

  :test-paths     ["test"]

  :resource-paths ["resources"]

  :clean-targets ^{:protect false} [:target-path :compile-path "resources/public/js"]

  :uberjar-name "inspection.jar"

  :main inspection.server.system

  :repl-options {:init-ns user}

  :cljsbuild {:builds
              [{:id "test-shared"
                :source-paths ["src/inspection/browser" "src/inspection/shared" "test/inspection/test/shared"]
                :compiler     {:output-to "resources/public/js/compiled/test_shared.js"
                               :main inspection.shared.test-runner
                               :optimizations :none}}
               {:id           "home"
                :source-paths ["src/inspection/browser" "src/inspection/shared"]
                :jar          true
                :compiler     {:main                 inspection.browser.home
                               :output-to            "resources/public/js/compiled/inspection.js"
                               :output-dir           "target"
                               :source-map-timestamp true
                               :optimizations        :advanced
                               :pretty-print         false}}]}
  :profiles {:dev
             {:dependencies [[figwheel "0.5.13"]
                             [figwheel-sidecar "0.5.13"]
                             [com.cemerick/piggieback "0.2.2"]
                             [org.clojure/tools.nrepl "0.2.13"]
                             [reloaded.repl "0.2.3"]]

              :plugins [[lein-figwheel   "0.5.10"]
                        [venantius/ultra "0.5.1"]]

              :source-paths ["dev" "test/shared"]
              :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}}

             :uberjar
             {:source-paths ^:replace ["src/inspection/server" "src/inspection/shared"]
              :prep-tasks ["compile"
                           ["cljsbuild" "once" "min"]
                           ["run" "-m" "garden-watcher.main" "inspection.assets"]]
              :hooks []
              :omit-source true
              :aot :all}})
