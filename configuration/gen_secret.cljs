#!/usr/bin/lumo

(defonce fs (js/require "fs"))
(defonce os (js/require "os"))

(let [home (.homedir os)]
  (.writeFileSync
    fs
    (str home "/.secrets.edn")
    (prn-str
      {:jwt-secret
       (str (random-uuid))})))
