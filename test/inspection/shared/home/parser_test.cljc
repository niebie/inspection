(ns inspection.shared.home.parser-test
  (:require
   [clojure.test :refer :all]
   [inspection.shared.home.parser :as parser]))

;; Read Tests
(deftest parse-current-user
  (let [test-user   "Test"
        input-key   :user/current-user
        environment {:state (atom {input-key test-user})}
        params      {}]
    (is (= {:value test-user} (parser/read environment input-key params)))))

(deftest parse-unassigned-key
  (let [test-value "Test"
        input-key   :test/unassigned-read
        environment {:state (atom {input-key test-value})}
        params      {}]
    (is (= {:value test-value} (parser/read environment input-key params)))))

;; Mutation Tests
(deftest parse-unassigned-mutation
  (is (= {:value {:error "no keys found"}} (parser/mutate (atom {}) :test/unassigned-mutation {}))))
