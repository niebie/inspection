(ns inspection.shared.test-runner
  (:require
   [clojure.test :refer :all]
   [inspection.shared.home.parser-test]))

(run-all-tests)
