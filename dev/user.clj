(ns user
  (:require [inspection.server.system :as main-system]
            [com.stuartsierra.component :as component]
            [figwheel-sidecar.config :as fw-config]
            [figwheel-sidecar.system :as fw-sys]
            [clojure.tools.namespace.repl :refer [set-refresh-dirs]]
            [reloaded.repl :refer [system init]]
            [figwheel-sidecar.repl-api :as figwheel]
            [garden-watcher.core :refer [new-garden-watcher]]))

(defn dev-system []
  (assoc (main-system/dev-system)
    :figwheel-system (fw-sys/figwheel-system (fw-sys/fetch-config))
    :css-watcher (fw-sys/css-watcher {:watch-paths ["resources/public/css"]})
    :garden-watcher (new-garden-watcher '[inspection.assets.home
                                          inspection.assets.unauthenticated])))

(set-refresh-dirs
  "src/inspection/server"
  "src/inspection/shared"
  "src/inspection/assets"
  "dev")

(reloaded.repl/set-init! #(dev-system))

(defn cljs-repl []
  (fw-sys/cljs-repl (:figwheel-system system)))

(defn cljs-repl-home
  []
  (fw-sys/cljs-repl (:figwheel-system system) "home"))

(defn cljs-repl-unauthenticated
  []
  (fw-sys/cljs-repl (:figwheel-system system) "unauthenticated"))

;; Set up aliases so they don't accidentally
;; get scrubbed from the namespace declaration
(def start reloaded.repl/start)
(def stop reloaded.repl/stop)
(def go reloaded.repl/go)
(def reset reloaded.repl/reset)
(def reset-all reloaded.repl/reset-all)
