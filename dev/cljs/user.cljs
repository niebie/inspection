(ns cljs.user
  (:require
   [inspection.shared.system :as system]))

(def init system/init)
(def start system/start)
(def stop system/stop)
(def reset system/reset)
(def go system/go)

(defn home-reset
  []
  (do
    (when-not system/new-system (system/new-home-system))
    (reset)))

(defn unauthenticated-reset
  []
  (do
    (when-not system/new-system (system/new-unauthenticated-system))
    (reset)))
