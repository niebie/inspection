(ns inspection.shared.home.parser
  (:require
   [inspection.shared.home.components.router :refer [component-map]]
   [om.next :as om]))

;; Reads
(defmulti read om/dispatch)

(defmethod read :default
  [{:keys [state]} input-key _]
  (let [state-deref @state
        value (input-key state-deref)]
    {:value value}))

;; Mutates
(defmulti mutate om/dispatch)

(defmethod mutate :default
  [_ _ _]
  {:value {:error "no keys found"}})
