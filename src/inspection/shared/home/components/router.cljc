(ns inspection.shared.home.components.router
  (:require
   [bidi.bidi :as bidi]
   [inspection.shared.home.components.about :refer [AboutPage about-page]]
   [inspection.shared.home.components.index :refer [IndexPage index-page]]
   [om.next :as om]
   #?(:cljs [pushy.core :as pushy])))

(defonce home-routes
  ["/" {""     :route/home
        "home" :route/home}])

(defonce component-map
  {:route/home  {:application/component IndexPage
                 :application/factory   index-page}
   :route/about {:application/component AboutPage
                 :application/factory   about-page}})

(defn set-route!
  [reconciler matched-token]
  (om/transact!
    reconciler
    `[(application/set-route! {:application/input-route ~matched-token}) :application/current-page]))

(defn- router-set-route!
  [reconciler {:keys [handler]}]
  (set-route! reconciler handler))

#?(:cljs
   (def history (atom nil)))

#?(:cljs
   (defn make-history!
     [reconciler]
     (reset! history
       (pushy/pushy
         (partial router-set-route! reconciler)
         (partial bidi/match-route home-routes)))))

#?(:cljs
   (defn begin-history!
     [reconciler]
     (do
       (make-history! reconciler)
       (pushy/start! @history))))

#?(:cljs
   (defn stop-history!
     []
     (let [history-ref @history]
       (when (not= nil history-ref)
         (reset! history (pushy/stop! history-ref))))))
