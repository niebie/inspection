(ns inspection.shared.home.components.about
  (:require
   [om.dom :as dom]
   [om.next :as om :refer [defui]]))

(defui AboutPage
  Object
  (render
    [this]
    (dom/div nil
      (dom/header #js {:id "header"}
        (dom/h1 nil "Cool ABOUT page")
        (dom/p nil "Wow such about page.")))))

(def about-page (om/factory AboutPage))
