(ns inspection.shared.home.components.index
  (:require
   [om.dom :as dom]
   [om.next :as om :refer [defui]]))

(defui IndexPage
  Object
  (render
    [this]
    (dom/div nil
      (dom/header #js {:id "header"}
        (dom/h1 nil "Reloading into windows")
        (dom/p nil "Welcome to the home page.")))))

(def index-page (om/factory IndexPage))
