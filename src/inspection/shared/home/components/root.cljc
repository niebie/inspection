(ns inspection.shared.home.components.root
  (:require
   [inspection.shared.components.navigation :as navigation :refer [NavigationBar]]
   [om.dom :as dom]
   [om.next :as om :refer [defui]]))

(defui RootComponent
  static om/IQuery
  (query
    [_]
    [{:application/navigation-data (om/get-query NavigationBar)}])
  Object
  (render
    [this]
    (let [{:keys [application/navigation-data]} (om/props this)
          {:keys [factory owner props]}         (om/get-computed this)]
      (dom/div nil
        (navigation/navigation-factory navigation-data)
        (factory props)))))
