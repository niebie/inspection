(ns inspection.shared.home.app
  (:require
   #?@(:cljs [[bidi.bidi :as bidi]
              [goog.dom :as gdom]
              [pushy.core :as pushy]])
   [com.stuartsierra.component :as component :refer [Lifecycle using]]
   [compassus.core :as compassus]
   [inspection.shared.logging :as logging]
   [inspection.shared.home.components.about :refer [AboutPage]]
   [inspection.shared.home.components.index :refer [IndexPage]]
   [inspection.shared.home.components.root :refer [RootComponent]]
   [inspection.shared.home.parser :as parser]
   [inspection.shared.utility :as utility]
   [om.next :as om]))

(defonce home-routes
  ["/" [["home"  :route/home]
        [""      :route/home]
        ["about" :route/about]]])

(def route-map
  {:route/home  IndexPage
   :route/about AboutPage})

#?(:cljs
   (declare app))

#?(:cljs
   (defn update-route!
     [{:keys [handler] :as route}]
     (let [current-route (compassus/current-route app)]
       (when (not= handler current-route)
         (compassus/set-route! app handler)))))

#?(:cljs
   (def history
     (pushy/pushy update-route!
       (partial bidi/match-route home-routes))))

#?(:cljs
   (defn get-route
     [route-key]
     (bidi/path-for home-routes route-key)))

#?(:cljs
   (defn set-route!
     [route]
     (pushy/set-token! history route)))

#?(:clj
   (defn make-app
     [send-fn]
     (compassus/application
       {:routes     route-map
        :reconciler (om/reconciler
                      {:normalize  true
                       :parser     (compassus/parser {:read parser/read :mutate parser/mutate})
                       :send       send-fn
                       :state      (atom {})})
        :mixins     [(compassus/wrap-render RootComponent)]}))
   :cljs
   (def app
     (compassus/application
       {:routes    route-map
        :reconciler (om/reconciler
                      {:logger     logging/logger
                       :normalize  true
                       :parser     (compassus/parser {:read parser/read :mutate parser/mutate})
                       :send       (utility/transit-post "/home/sync")
                       :shared     {:shared/get-route get-route
                                    :shared/set-route! set-route!}
                       :state      (atom {:user/current-user {:user/first-name "Devo"
                                                              :user/last-name  "Henley"}})})
        :mixins    [(compassus/did-mount (fn [_]
                                           (pushy/start! history)))
                    (compassus/will-unmount (fn [_]
                                              (pushy/stop! history)))
                    (compassus/wrap-render RootComponent)]})))

(defn mount!
  #?(:clj [component]
     :cljs [component app-element])
  (compassus/mount!
    component
    #?(:clj nil
       :cljs (gdom/getElement app-element))))

#?(:clj
   (defn mount-app!
     [send-fn]
     (mount! (make-app send-fn)))
   :cljs
   (defn mount-app!
     [app-element]
     (mount! app app-element)))

(defrecord HomeApp [send-fn app-element app]
  Lifecycle
  (start
    [this]
    (assoc this :app
      #?(:clj  (mount-app! send-fn)
         :cljs (mount-app! app-element))))
  (stop
    [this]
    (assoc this :app nil)))

(defn make-app
  []
  (using
    (map->HomeApp {})
    [:app-element]))
