(ns inspection.shared.unauthenticated.components.page
  (:require
   [inspection.shared.utility :as utility]
   [om.dom :as dom]
   [om.next :as om :refer [defui IQuery IQueryParams]]))

(defn login!
  [component]
  (let [{:keys [email password]} (om/get-params component)]
    (om/transact!
      component
      `[(authentication/login!
          {:email ~email
           :password ~password})
        :authentication/failed?])))

(defn make-params
  [this base-params path]
  #?(:clj base-params
     :cljs (clj->js
             (merge
               base-params
               {:onChange (partial
                            utility/update-param-handler
                            this
                            path)}))))

(defn make-email-params
  [this]
  (let [base-params {:className   "account-email-input"
                     :placeholder "Email"
                     :type        "email"}]
    (make-params this base-params [:email])))

(defn make-password-params
  [this]
  (let [base-params {:className   "password-input"
                     :placeholder "Password"
                     :type        "password"}]
    (make-params this base-params [:password])))

(defui UnauthenticatedPage
  static IQueryParams
  (params
    [this]
    {:email ""
     :password ""})
  static IQuery
  (query
    [this]
    [:authentication/failed?
     '(:email/valid-email? {:email ?email})
     '(:password/valid-password? {:password ?password})])
  Object
  (render
    [this]
    (let [{:keys [authentication/failed?
                  email/valid-email?
                  password/valid-password?]} (om/props this)]
      (dom/div #js {:className "login-page"}
        (dom/h1 #js {:className "login-header"} "Welcome to Inspection")
        (dom/div #js {:className "login-form"}
          (dom/div #js {:className "login-form-field"}
            (dom/input (make-email-params this))
            (when-not valid-email? (dom/p nil "This email does not appear to be valid.")))
          (dom/div #js {:className "login-form-field"}
            (dom/input (make-password-params this))
            (when-not valid-password? (dom/p nil "This password does not appear to be valid.")))
          (dom/div #js {:className "login-button-wrapper"}
            (dom/button #js {:className "login-button"
                             :onClick (partial login! this)}
              "Log In")))
        (dom/div #js {:className "login-error-container"}
          (dom/p nil
            (if failed?
              "Login failed. Please check that your email and password are correct."
              "")))
        (dom/div #js {:className "registration-link-container"}
          (dom/p #js {:className "registration-text"} "Not registered?"
            (dom/a #js {:className "registration-link"} "Create an account")))))))
