(ns inspection.shared.unauthenticated.parser
  (:require
   #?(:clj [buddy.sign.jwt :as jwt])
   #?(:clj [clj-time.core :as time])
   #?(:clj [inspection.server.database.protocols :refer [login!]])
   #?(:cljs [goog.net.cookies :as cookies])
   [inspection.shared.utility :as utility]
   [om.next :as om]))

;; Reads
(defmulti read utility/dispatch)

#?(:clj
   (defmethod read :default
     [cmap {:keys [state]} input-key _]
     (let [state-deref @state
           value (input-key state-deref)]
       {:value value}))
   :cljs
   (defmethod read :default
     [{:keys [state]} input-key _]
     (let [state-deref @state
           value (input-key state-deref)]
       {:value value})))

#?(:clj
   (defmethod read :authentication/failed?
     [_ _ _ _]
     {:value false}))

#?(:cljs
   (defmethod read :authentication/token
     [_ {:keys [state]} input-key _]
     (let [state-deref @state
           value (input-key state-deref)]
       (do
         (when value
           (.set
             goog.net.cookies
             "session"
             "value"
             -1))
         {:value value}))))

#?(:cljs
   (defmethod read :email/valid-email?
     [_ _ {:keys [email]}]
     {:value (boolean
               (re-matches
                 #"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"
                 email))}))

#?(:cljs
   (defmethod read :password/valid-password?
     [_ _ {:keys [password]}]
     {:value (boolean
               (re-matches
                 #"^(?!.*([A-Za-z0-9])\1{2})(?=.*[a-z])(?=.*\d)[A-Za-z0-9]{8,16}$"
                 password))}))

;; Mutates
(defmulti mutate utility/dispatch)

#?(:clj
   (defmethod mutate :default
     [_ _ _ _]
     {:value {:error "no keys found"}})
   :cljs
   (defmethod mutate :default
     [_ _ _]
     {:value {:error "no keys found"}}))

#?(:clj
   (defmethod mutate 'authentication/login!
     [{:keys [database authentication]}
      {:keys [state]}
      input-key
      {:keys [email password]}]
     (if (login! database email password)
       {:value {:keys [:authentication/token]}
        :action #(let [token (jwt/sign {:claims (pr-str {:email email :roles #{:user}})
                                        :exp (time/plus (time/now) (time/hours 8))})]
                   (swap!
                     state
                     assoc-in
                     [:authentication/token]
                     token))}
       {:value {:keys [:authentication/failed?]
                :action #(swap!
                           state
                           assoc-in
                           [:authentication/failed?]
                           true)}}))
   :cljs
   (defmethod mutate 'authentication/login!
     [env input-key params]
     {:value {:keys [:authentication/token :authentication/failed?]}
      :remote true}))
