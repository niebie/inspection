(ns inspection.shared.unauthenticated.app
  (:require
   #?(:cljs [goog.dom :as gdom])
   [com.stuartsierra.component :refer [Lifecycle using]]
   [inspection.shared.unauthenticated.components.page :refer [UnauthenticatedPage]]
   [inspection.shared.unauthenticated.parser :as parser]
   #?(:cljs [inspection.shared.utility :refer [transit-post]])
   [om.next :as om]))

#?(:clj
   (defn make-reconciler
     [send-fn component-map]
     (om/reconciler
       {:normalize true
        :parser (om/parser {:read (partial parser/read component-map)
                            :mutate (partial parser/mutate component-map)})
        :state (atom {})}))
   :cljs
   (def app-reconciler
     (om/reconciler
       {:normalize true
        :send (transit-post "/authentication/sync")
        :parser (om/parser {:read parser/read :mutate parser/mutate})
        :state (atom {})})))

(defn mount!
  #?(:clj [reconciler]
     :cljs [reconciler app-element])
  (om/add-root!
    reconciler
    UnauthenticatedPage
    #?(:clj nil
       :cljs (gdom/getElement app-element))))

#?(:clj
   (defn mount-app!
     [send-fn component-map]
     (mount! (make-reconciler send-fn component-map)))
   :cljs
   (defn mount-app!
     [app-element]
     (mount! app-reconciler app-element)))

(defrecord UnauthenticatedApp
    #?(:clj [app-element app send-fn component-map]
       :cljs [app-element app])
  Lifecycle
  (start
    [this]
    (assoc this :app
      #?(:clj (mount-app! send-fn component-map)
         :cljs (mount-app! app-element))))
  (stop
    [this]
    (assoc this :app nil)))

(defn make-app
  []
  (using
    (map->UnauthenticatedApp {})
    [:app-element]))
