(ns inspection.shared.components.navigation
  (:require
   [om.dom :as dom]
   [om.next :as om :refer [defui]]))

(defui NavigationBar
  static om/IQuery
  (query
    [_]
    '[[:user/current-user _]])
  Object
  (render
    [this]
    (let [{:keys [shared/get-route
                  shared/set-route!] :as shared} (om/shared this)]
      (dom/nav #js {:className "navigation"}
        (when-let [{:keys [user/current-user]} (om/props this)]
          (dom/div #js {:className "navigation-user-display"}
            (let [{:keys [user/first-name
                          user/last-name]} current-user]
              (dom/div #js {:className "name-display"} (str "Hello " first-name " " last-name)))))
        (dom/div #js {:className "navigation-buttons"}
          (dom/button #js {:className "navigation-button"
                           :onClick #(set-route! (get-route :route/home))} "Home")
          (dom/button #js {:className "navigation-button"
                           :onClick #(set-route! (get-route :route/about))} "About")
          (dom/button #js {:className "navigation-button"} "DevCards"))))))

(def navigation-factory (om/factory NavigationBar))
