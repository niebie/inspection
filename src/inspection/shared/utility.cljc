(ns inspection.shared.utility
  (:require
   #?@(:cljs [[ajax.core :refer [GET POST]]])
   [cognitect.transit :as transit]
   [om.next :as om]))

(def dispatch
  #?(:clj (fn [component-map env key params] key)
     :cljs om/dispatch))

(defn update-param!
  [component path value]
  (om/update-query!
    component
    (fn [state]
      (assoc-in state (into [] (cons :params path)) value))))

#?(:cljs
   (defn update-param-handler
     [component path event]
     (update-param!
       component
       path
       (.. event -target -value))))

#?(:cljs
   (defn transit-post
     [url]
     (fn [{:keys [remote] :as env} post-callback]
       (POST url
         {:handler (fn [response]
                     (post-callback response))
          :error-handler (fn [response])
          :body (transit/write (transit/writer :json) remote)
          :format :transit
          :params :transit
          :response-format :transit
          :with-credentials true
          :headers {:content-type "application/transit+json"}}))))
