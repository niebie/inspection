(ns inspection.shared.system
  (:require
   [com.stuartsierra.component :as component]
   [inspection.shared.home.app :as home]
   [inspection.shared.unauthenticated.app :as unauthenticated]))

(declare system)
(declare new-system)

(defn new-generic-system
  [make-app]
  (component/system-map
    :app-element "app"
    :app (make-app)))

(defn new-home-system
  []
  (set! new-system (partial new-generic-system home/make-app)))

(defn new-unauthenticated-system
  []
  (set! new-system (partial new-generic-system unauthenticated/make-app)))

(defn init
  []
  (set! system (new-system)))

(defn start
  []
  (set! system (component/start system)))

(defn stop
  []
  (set! system (component/stop system)))

(defn ^:export go
  []
  (init)
  (start))

(defn reset
  []
  (stop)
  (go))
