(ns inspection.server.database
  (:require
   [buddy.hashers :refer [derive check]]
   [clojure.java.io :as io]
   [com.stuartsierra.component :as component]
   [clojure.core.async :as async :refer [<!!]]
   [datomic.api :as datomic]
   [datomic.client :as client]
   [inspection.server.database.protocols :as protocols]
   [io.rkn.conformity :as conformity]))

(defrecord Database [database-uri connection schema profile]
  component/Lifecycle
  (start
    [component]
    (datomic/create-database database-uri)
    (let [connection (datomic/connect database-uri)]
      (conformity/ensure-conforms
        connection
        schema
        [:inspection/initial-schema])
      (assoc component :connection connection)))
  (stop
    [component]
    (when (= profile :dev)
      (datomic/delete-database database-uri))
    (assoc component :connection nil))
  protocols/Authenticator
  (login!
    [component email password]
    (let [db (client/db connection)
          user-hash (<!! (client/q
                           connection
                           {:query '[:find ?password-hash .
                                     :in $ ?input-email
                                     :where
                                     [?e :user/account-email ?input-email]
                                     [?e :user/password ?password-hash]]
                            :args [db email]}))]
      (check password user-hash))))

(defn new-database
  []
  (map->Database {}))
