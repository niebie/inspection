(ns inspection.server.resources.home
  (:require
   [clojure.spec.alpha :as spec]
   [hiccup.core :as hiccup]
   [hiccup.page :as page]
   [inspection.server.resources.utilities :as utilities]
   [inspection.shared.home.app :as app]
   [om.dom :as dom]
   [yada.yada :as yada]))

(defn home-page
  [database]
  (page/html5
    [:head
     [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
     [:meta {:charset "utf-8"}]
     [:meta {:http-equiv "X-UA-Compatible"}]
     (page/include-css "/css/home-styles.css")
     [:title "Inspection Homepage"]]
    [:body
     [:section#app
      (dom/render-to-str (app/mount-app! (fn [a] "hello")))]
     (page/include-js "/js/compiled/home.js")]))

(defn home-resource
  [database-component authentication-component]
  (yada/resource
    (utilities/merge-standard-responses
      {:id :inspection.resources/home
       :description "Content routes for the home section of the application."
       :access-control (utilities/make-default-access-control-map
                         authentication-component
                         {:methods {:get :user}})
       :produces [{:media-type #{"text/html"}
                   :charset    "UTF-8"}]
       :methods
       {:get {:response (fn [context]
                          (home-page database-component))}}}
      database-component
      authentication-component)))
