(ns inspection.server.resources.authentication
  (:require
   [buddy.sign.jwt :as jwt]
   [clj-time.core :as time]
   [inspection.server.resources.constants :as constants :refer [standard-inputs]]
   [inspection.server.resources.utilities :as utilities]
   [schema.core :as schema :refer [required-key defschema]]
   [yada.yada :as yada]))

(defschema UserAuthentication
  {:username String
   :password String})

(defn- make-session-cookie
  [database-component authentication-component uuid]
  {"session" {:path "/"
              :value (jwt/sign {:claims (pr-str {:uuid uuid
                                                 :roles #{:user}})
                                :exp (time/plus (time/now) (time/hours 2))}
                       (:secret authentication-component))}})

(defn- authentication-response
  [database-component authentication-component context]
  (let [{:keys [username password] :as credentials} (get-in context [:parameters :body])
        [uuid authenticated?] [1 true]
        secret (:secret authentication-component)]
    (merge
      (:response context)
      (if-not authenticated?
        {:status 401}
        {:status 200
         :cookies (make-session-cookie
                    database-component
                    authentication-component
                    uuid)}))))

(defn make-authentication-resource
  [database-component authentication-component]
  (yada/resource
    {:description "Authentication routes."
     :produces [{:media-type #{"text/plain"}
                 :charset    "UTF-8"}]
     :methods
     {:post
      {:consumes standard-inputs
       :produces "text/plain"
       :parameters {:body UserAuthentication}
       :response (partial authentication-response database-component authentication-component)}}}))
