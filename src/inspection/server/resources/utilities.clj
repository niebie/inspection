(ns inspection.server.resources.utilities
  (:require
   [inspection.server.authentication.protocols :refer [get-claims]]
   [inspection.server.resources.generic.unauthenticated :as unauthenticated]))

(defmacro inject-components
  [database-component authentication-component response-map]
  (reduce-kv
    (fn [response-map key [resource-func & args]]
      (assoc response-map key (list
                                apply
                                resource-func
                                database-component
                                authentication-component
                                args)))
    {}
    response-map))

(defn make-default-access-control-map
  [authentication-component methods-map]
  {:scheme :cookie
   :verify (partial get-claims authentication-component)
   :authorization methods-map})

(defn merge-standard-responses
  [response database-component authentication-component]
  (merge
    response
    {:responses
     (inject-components
       database-component
       authentication-component
       {401 (unauthenticated/make-unauthenticated-resource)})}))
