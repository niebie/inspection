(ns inspection.server.resources.generic.unauthenticated
  (:require
   [hiccup.page :as page]
   [inspection.shared.unauthenticated.app :as app]
   [om.dom :as dom]))

(defn make-unauthenticated-page
  [context database-component authentication-component]
  (page/html5
    [:head
     [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
     [:meta {:charset "utf-8"}]
     [:meta {:http-equiv "X-UA-Compatible"}]
     [:link {:href "https://fonts.googleapis.com/css?family=Source+Sans+Pro" :rel "stylesheet"}]
     (page/include-css "/css/unauthenticated-styles.css")
     [:title "Unauthenticated!"]]
    [:body
     [:section#app
      (dom/render-to-str (app/mount-app!
                           (fn [ks cb] "hello")
                           {:database database-component
                            :authentication authentication-component}))]
     (page/include-js "/js/compiled/unauthenticated.js")]))

(defn make-unauthenticated-resource
  [database-component authentication-component]
  {:description "Login page response for whenever a user is unauthenticated."
   :produces #{"text/html" "text/plain;q=0.9"}
   :response (fn [context]
               (make-unauthenticated-page
                 context
                 database-component
                 authentication-component))})
