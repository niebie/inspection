(ns inspection.server.routing
  (:require
   [clojure.java.io :as io]
   [inspection.server.routes.authentication :as authentication]
   [inspection.server.routes.home :as home]
   [yada.yada :as yada]))

(defn content-routes
  []
  [""
   [
    ["/"
     (->
       (yada/as-resource (io/file "resources/public"))
       (assoc :id :inspection.resources/public))]]])

(defn routes
  [database-component config]
  [""
   [
    (home/make-home-routes database-component {})
    (authentication/make-authentication-routes database-component {})
    (content-routes)
    [true (yada/handler nil)]
    ]])
