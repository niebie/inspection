(ns inspection.server.routes.authentication
  (:require
   [inspection.server.resources.authentication :as authentication]
   [yada.yada :as yada]))

(defn make-authentication-routes
  [database-component authentication-component]
  (let [authentication-routes ["/"
                               [
                                ["login" (authentication/make-authentication-resource
                                           database-component
                                           authentication-component)]]]]
    [""
     [
      authentication-routes
      ]]))
