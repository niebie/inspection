(ns inspection.server.routes.home
  (:require
   [inspection.server.resources.home :as home]))

(defn make-home-routes
  [database-component authentication-component]
  (let [home-routes ["/"
                     [
                      ["home" (home/home-resource database-component authentication-component)]
                      ]]]
    [""
     [
      home-routes
      ]]))
