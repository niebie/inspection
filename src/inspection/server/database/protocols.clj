(ns inspection.server.database.protocols)

(defprotocol Authenticator
  (login!
    [component email password]
    "Logs a user in, returning true if the account exists and false if it does not."))
