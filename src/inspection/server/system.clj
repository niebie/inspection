(ns inspection.server.system
  (:require
   [aero.core :as aero :refer [root-resolver]]
   [clojure.java.io :as io]
   [com.stuartsierra.component :as component :refer [system-map system-using]]
   [inspection.server.authentication.component :refer [new-authenticator]]
   [inspection.server.database :refer [new-database]]
   [inspection.server.web-server :refer [new-server]])
  (:gen-class))

(def system nil)

(defn config
  [profile]
  (aero/read-config "configuration/config.edn" {:profile profile
                                                :resolver root-resolver}))

(defn configure-components
  [system config]
  (merge-with merge system config))

(defn new-system-map
  [config profile]
  (system-map
    :server   (new-server)
    :database (new-database)
    :authenticator (new-authenticator)
    :profile  profile))

(defn new-dependency-map
  []
  {})

(defn new-system
  [profile]
  (let [config (config profile)]
    (->
      (new-system-map config profile)
      (configure-components config)
      (system-using (new-dependency-map)))))

(defn dev-system
  []
  (new-system :dev))

(defn -main
  [& args]
  (let [system (new-system :prod)]
    (component/start system))
  @(promise))
