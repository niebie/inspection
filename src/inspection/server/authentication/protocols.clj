(ns inspection.server.authentication.protocols)

(defprotocol Authentication
  (get-claims
    [component cookie]
    "Gets the claims from an input cookie."))
