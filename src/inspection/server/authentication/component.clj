(ns inspection.server.authentication.component
  (:require
   [buddy.sign.jwt :as jwt]
   [clojure.edn :as edn]
   [com.stuartsierra.component :refer [Lifecycle]]
   [inspection.server.authentication.protocols :refer [Authentication]]))

(defrecord AuthenticationComponent [jwt-secret]
  Lifecycle
  (start
    [this]
    this)
  (stop
    [this]
    this)
  Authentication
  (get-claims
    [this cookie]
    (some->
      cookie
      (jwt/unsign jwt-secret)
      :claims
      edn/read-string)))

(defn new-authenticator
  []
  (map->AuthenticationComponent {}))
