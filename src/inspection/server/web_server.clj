(ns inspection.server.web-server
  (:require
   [bidi.vhosts :refer [vhosts-model]]
   [clojure.spec.alpha :as s]
   [clojure.tools.logging :refer :all]
   [com.stuartsierra.component :refer [Lifecycle using]]
   [inspection.server.routing :refer [routes]]
   [yada.yada :as yada]))

(s/def :unq/port integer?)
(s/def :unq/listener (s/keys :req-un [:unq/port :unq/close :unq/webserver]))
(s/def ::webserver (s/keys :req-un [:unq/port :unq/listener]))

(defrecord Server
    [port
     database
     jwt-secret
     listener]
  Lifecycle
  (start
    [component]
    (if
        listener
      component
      (let
          [vhosts-model (vhosts-model
                          [{:scheme :http :host (format "localhost:%d" port)}
                           (routes database {:port       port
                                             :jwt-secret jwt-secret})])
           listener (yada/listener vhosts-model {:port port})]
        (infof "Started server on port %s" (:port listener))
        (assoc component :listener listener))))
  (stop
    [component]
    (when-let
        [close (get-in component [:listener :close])]
      (close))
    (assoc component :listener nil)))

(defn new-server
  []
  (using
    (map->Server {})
    [:database
     :jwt-secret]))
