(ns inspection.assets
  (:require
   [inspection.assets.home :as home-css]
   [inspection.assets.unauthenticated :as unauthenticated-css]))

(def ^:garden home-styles
  home-css/home-styles)

(def ^:garden unauthenticated-styles
  unauthenticated-css/unauthenticated-styles)
