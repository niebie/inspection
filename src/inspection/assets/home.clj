(ns inspection.assets.home)

(def ^:garden home-styles
  [
   [:#app {:padding-top "15px"
           :padding-left "40px"
           :padding-right "40px"}]
   [:.navigation {:display "flex"
                  :flex-direction "row"
                  :height "5vh"
                  :justify-content "space-between"}]
   [:.navigation-buttons {:height "100%"}]
   [:.navigation-button {:margin-left "20px"}]])
