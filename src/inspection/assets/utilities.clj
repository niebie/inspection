(ns inspection.assets.utilities
  (:require
   [garden.stylesheet :refer [at-media]]))

(defmacro insert-element
  [element & css]
  (mapv (fn [[media-query css-map]] (list media-query [element css-map])) css))

(def mobile-label-font-size "14px")

(def tablet-label-font-size "16px")

(def computer-label-font-size "20px")

(def retro-green-text "rgba(82, 196, 138, .97)")
(def retro-green-glowing-text "rgba(18, 255, 169, .97)")
(def retro-green-text-shadow "0px 0px 15px rgba(34, 255, 169, 1)")

(def input-border-color retro-green-text)

(def background-color "black")
(def border-color retro-green-text)

(defn at-mobile-media
  [css]
  (at-media {:max-width "479px"}
    css))

(defn at-tablet-media
  [css]
  (at-media {:min-width "480px"
             :max-width "768px"}
    css))

(defn at-computer-media
  [css]
  (at-media {:min-width "769px"}
    css))

(defn at-standard-media
  [element mobile-css tablet-css computer-css]
  (insert-element
    element
    (at-mobile-media mobile-css)
    (at-tablet-media tablet-css)
    (at-computer-media computer-css)))
