(ns inspection.assets.unauthenticated
  (:require
   [garden.selectors :as selectors]
   [inspection.assets.utilities :as util]))

(selectors/defpseudoelement placeholder)

(def login-form-width "230px")

(def ^:garden unauthenticated-styles
  [
   [:body {:height "100vh"
           :width "100vw"
           :font-family "Source Sans Pro"
           :background-color util/background-color}]
   [:section#app {:display "flex"
                  :flex-direction "column"
                  :justify-content "center"
                  :align-items "center"
                  :height "100%"
                  :width "100%"}]
   [:button.login-button {:background util/background-color
                          :border-color util/border-color
                          :color util/retro-green-glowing-text}
    [:&:focus {:border-color util/input-border-color
               :outline "0"}]]
   [:div.login-page {:background util/background-color
                     :display "flex"
                     :flex-direction "column"
                     :justify-content "center"
                     :align-items "center"
                     :width "35vw"
                     :min-width "450px"
                     :height "45vh"
                     :border-style "solid"
                     :border-width "3px"
                     :border-color util/border-color}]
   [:div.login-form {:display "flex"
                     :flex-direction "column"
                     :justify-content "center"
                     :align-items "center"}]
   [:div.login-form-field {:display "flex"
                           :flex-direction "column"
                           :justify-content "center"
                           :align-items "center"
                           :margin-top "10px"
                           :margin-bottom "10px"
                           :width "325px"}
    [:input {:width login-form-width
             :background-color util/background-color
             :border-color util/input-border-color
             :color util/retro-green-glowing-text
             :caret-color util/retro-green-glowing-text}]
    [:p {:width login-form-width
         :background-color util/background-color
         :border-color util/input-border-color
         :color util/retro-green-glowing-text}]
    [(selectors/selector (selectors/input placeholder)) {:color util/retro-green-glowing-text
                                                         :text-shadow util/retro-green-text-shadow}]]
   [:p.registration-text {:color util/retro-green-glowing-text}]
   [:a.registration-link {:margin-left "10px"
                          :font-weight "bold"
                          :color util/retro-green-glowing-text
                          :text-shadow util/retro-green-text-shadow}]
   [:.login-header {:color util/retro-green-glowing-text
                    :text-shadow util/retro-green-text-shadow}]])
